select t.id, t.heads, t.arms, b.legs, b.tails, CASE
WHEN t.heads > t.arms OR b.tails > b.legs
THEN 'BEAST'
ELSE 'WEIRDO'
END species
from top_half t
join bottom_half b on b.id = t.id order by species